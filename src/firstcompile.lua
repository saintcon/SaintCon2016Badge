--Example: require("initcompile")


local function compileremove(name)
	return function()
		if string.find(name,"^init") == nil then
			if string.find(name,".lua$") ~= nil then
				file.remove(string.gsub(name, ".lua$", ".lc"))
				print("Compiling: " .. name)
				node.compile(name)
				file.remove(name)
			end
		else
			print("Skipping: " .. name)
		end
	end
end


--Will crash if there is more to compile than queue room. Custom build increases
--queue length from 8 to 16. Would be nice to find a way to break up tasks into
--larger chunks.
local flist = file.list()
for n,v in pairs(flist) do
	node.task.post(node.task.MEDIUM_PRIORITY, compileremove(n))
end

flist = nil
