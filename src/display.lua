-- Really random test code for the MAx7219.
-- Chain 7 segment display off dev board
-- Chain 8x8 matix off of 7 segment display.
-- run this code to get 87654321. Assuming the L to R digit layout is just the
-- dev boards we picked up.

local moduleName = ...
local DISPLAY = {}

local MAX7219_REG_NOOP =   0x00;
local MAX7219_REG_DIGIT0 = 0x01;
local MAX7219_REG_DIGIT1 = 0x02;
local MAX7219_REG_DIGIT2 = 0x03;
local MAX7219_REG_DIGIT3 = 0x04;
local MAX7219_REG_DIGIT4 = 0x05;
local MAX7219_REG_DIGIT5 = 0x06;
local MAX7219_REG_DIGIT6 = 0x07;
local MAX7219_REG_DIGIT7 = 0x08;
local MAX7219_REG_DECODEMODE =  0x09;
local MAX7219_REG_INTENSITY =   0x0A;
local MAX7219_REG_SCANLIMIT =   0x0B;
local MAX7219_REG_SHUTDOWN =    0x0C;
local MAX7219_REG_DISPLAYTEST = 0x0F;

--- Send byte to specific chip
--
-- @param chip which chip to send data.
-- @param data a number representing command+data.
local function sendchipbyte(chip, data)
  if chip == 0 then
    -- set same data for both chips
    spi.set_mosi(1,0,16,data,data);
  elseif chip == 1 then
    -- set no-op (second chip) then data (first chip)
    spi.set_mosi(1,0,16,0x0000,data);
  elseif chip == 2 then
    -- set data (2nd chip) then no-op (first chip)
    spi.set_mosi(1,0,16,data,0x0000);
  else
    -- invalid choice send no-op. Should just do nothing here.
    spi.set_mosi(1,0,16,0x0000,0x0000);
  end
  spi.transaction(1,0,0,0,0,32,0,0);
end

function DISPLAY.clear()
  for i=1,8 do
    spi.send(1,i*256+15);
    tmr.delay(50);
  end
end

function DISPLAY.setup(boot)
  spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 32, 8);
  
  local b = rtcmem.read32(0);
  if file.exists("score.dsp") then
    spi.set_mosi(1,0,16,0x0, 0x0C00);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0B07);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x09FF);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0A00+b);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0C01);
    spi.transaction(1,0,0,0,0,32,0,0);
    tmr.stop(0);
    file.open("score.dsp", "r")
    DISPLAY.display(tonumber(file.read()))
    file.close()
  else
    spi.set_mosi(1,0,16,0x0, 0x0C00);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0B07);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0900);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0A00+b);
    spi.transaction(1,0,0,0,0,32,0,0);
    spi.set_mosi(1,0,16,0x0, 0x0C01);
    spi.transaction(1,0,0,0,0,32,0,0);

    DISPLAY.goodbye();
  end
  --end
end

function DISPLAY.brightness()
  node.task.post(node.task.MEDIUM_PRIORITY, function()
      http.get("http://10.90.0.16", "Accept: */*\r\n", function(code, data)
          if (code == 200 ) then
            print("SC+BRIGHT="..data)
            b = tonumber(data)
            if (b ~= nil and b <= 15) then
              rtcmem.write32(0, b);
              spi.send(1, 0x0A00 + b)
            end
          else
            print('http error '..code)
          end
        end)
    end)
end

function DISPLAY.saintcon()
  spi.send(1, {0x0900,0x0000,0x0000});
  spi.set_mosi(1,0,16,0x0, 0x0C00);
  spi.transaction(1,0,0,0,0,32,0,0);
  sendchipbyte(1, 0x015B);
  sendchipbyte(1, 0x0233);
  sendchipbyte(1, 0x0306);
  sendchipbyte(1, 0x0415);
  sendchipbyte(1, 0x0570);
  sendchipbyte(1, 0x060D);
  sendchipbyte(1, 0x077E);
  sendchipbyte(1, 0x0815);
  spi.set_mosi(1,0,16,0x0, 0x0C01);
  spi.transaction(1,0,0,0,0,32,0,0);
end

function DISPLAY.goodbye()
  spi.send(1, {0x0900,0x0000,0x0000});
  spi.set_mosi(1,0,16,0x0, 0x0C00);
  spi.transaction(1,0,0,0,0,32,0,0);
  sendchipbyte(1, 0x015f);
  sendchipbyte(1, 0x021d);
  sendchipbyte(1, 0x031d);
  sendchipbyte(1, 0x043d);
  sendchipbyte(1, 0x051f);
  sendchipbyte(1, 0x063b);
  sendchipbyte(1, 0x074f);
  sendchipbyte(1, 0x08A0);
  spi.set_mosi(1,0,16,0x0, 0x0C01);
  spi.transaction(1,0,0,0,0,32,0,0);
end

function DISPLAY.help()
  spi.send(1, {0x09FF, 0x0C01});
  spi.set_mosi(1,0,16,0x0, 1*256+10);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 2*256+10);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 3*256+12);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 4*256+11);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 5*256+13);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 6*256+14);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 7*256+10);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 8*256+10);
  spi.transaction(1,0,0,0,0,32,0,0);
end

function DISPLAY.display(num)
  if tonumber(num) ~= 0 then
    spi.send(1, {0x0C00, 0x09FF, 0x0C01});
    if string.len(num) <= 8 then
      num = tonumber(num)
      if num ~= nil then

        file.open("score.dsp", "w")
        file.write(num)
        file.close()

        DISPLAY.clear();
        local i = 8;
        while num > 0 do
          spi.set_mosi(1,0,16,0x0, i*256+(num % 10));
          spi.transaction(1,0,0,0,0,32,0,0);
          num = (num - (num %10)) / 10;
          i = i - 1;
        end
      end
    end
  end
end

function DISPLAY.unload()
  package.loaded[moduleName]=nil
  moduleName = nil
  DISPLAY = nil
end

return DISPLAY
