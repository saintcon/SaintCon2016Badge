-- Function to add scrolling text by magnusviri
-- Original Source: BlackBadge
-- Creator: KlintHolmes
-- Modified by Jonathan Karras to be a standalone module.

-- Usage:
-- require('knightrider')()

local moduleName = ...
return function()
  package.loaded[moduleName]=nil
  moduleName = nil
  spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 32, 8);
  spi.set_mosi(1,0,16,0x0, 0x0C00);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0B07);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0900);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0A15);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0C01);
  spi.transaction(1,0,0,0,0,32,0,0);

  local function loop()
    row = 0
    while row < 4 do
      spi.send(1, {0x0140});
      tmr.delay(50000);

      spi.send(1, {0x0240});
      tmr.delay(500);
      spi.send(1, {0x0100});
      tmr.delay(50000);

      spi.send(1, {0x0340});
      tmr.delay(500);
      spi.send(1, {0x0200});
      tmr.delay(50000);

      spi.send(1, {0x0440});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0540});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0640});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0740});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0840});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0740});
      tmr.delay(500);
      spi.send(1, {0x0800});
      tmr.delay(50000);

      spi.send(1, {0x0640});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0540});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0440});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0340});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0240});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0140});
      tmr.delay(500);
      spi.send(1, {0x0200});
      spi.send(1, {0x0100});
      row = row + 1
    end

    row = 0
    while row < 4 do
      spi.send(1, {0x0101});
      tmr.delay(50000);

      spi.send(1, {0x0201});
      tmr.delay(500);
      spi.send(1, {0x0100});
      tmr.delay(50000);

      spi.send(1, {0x0301});
      tmr.delay(500);
      spi.send(1, {0x0200});
      tmr.delay(50000);

      spi.send(1, {0x0401});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0501});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0601});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0701});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0801});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0701});
      tmr.delay(500);
      spi.send(1, {0x0800});
      tmr.delay(50000);

      spi.send(1, {0x0601});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0501});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0401});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0301});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0201});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0101});
      tmr.delay(500);
      spi.send(1, {0x0200});
      spi.send(1, {0x0100});
      row = row + 1
    end
    row = 0
    while row < 4 do
      spi.send(1, {0x0108});
      tmr.delay(50000);

      spi.send(1, {0x0208});
      tmr.delay(500);
      spi.send(1, {0x0100});
      tmr.delay(50000);

      spi.send(1, {0x0308});
      tmr.delay(500);
      spi.send(1, {0x0200});
      tmr.delay(50000);

      spi.send(1, {0x0408});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0508});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0608});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0708});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0808});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0708});
      tmr.delay(500);
      spi.send(1, {0x0800});
      tmr.delay(50000);

      spi.send(1, {0x0608});
      tmr.delay(500);
      spi.send(1, {0x0700});
      tmr.delay(50000);

      spi.send(1, {0x0508});
      tmr.delay(500);
      spi.send(1, {0x0600});
      tmr.delay(50000);

      spi.send(1, {0x0408});
      tmr.delay(500);
      spi.send(1, {0x0500});
      tmr.delay(50000);

      spi.send(1, {0x0308});
      tmr.delay(500);
      spi.send(1, {0x0400});
      tmr.delay(50000);

      spi.send(1, {0x0208});
      tmr.delay(500);
      spi.send(1, {0x0300});
      tmr.delay(50000);

      spi.send(1, {0x0108});
      tmr.delay(500);
      spi.send(1, {0x0200});
      spi.send(1, {0x0100});
      row = row + 1
    end
    -- Don't busy loop the ESP or it will crash.
    print("Restart Loop: "..node.heap())
    node.task.post(loop)
  end

  --Start loop
  print("Start Loop: "..node.heap())
  node.task.post(loop)
end
