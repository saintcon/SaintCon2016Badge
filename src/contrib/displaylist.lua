-- Send animations to 7-Segment display via table fed to function.
-- Table must be two dimensional. See knightrider2.lua for an example.
-- Inner list is in the form `innerlist1[4] = {0x0308,1}` where `0x038`
-- is the command to be sent via SPI and `1` is the delay in milliseconds
-- before advancing to the next command.

-- Call function using one of two forms below:
-- require('displaylist')(mylist);
-- or
-- runme = require('displaylist');
-- runme(mylist);

local moduleName = ...
return function (datalist, outerdelayms, callback)
  package.loaded[moduleName]=nil
  moduleName = nil
  outerdelayms = outerdelayms or 0; -- default value of 0ms;
  spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 32, 8);
  spi.set_mosi(1,0,16,0x0, 0x0C00);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0B07);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0900);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0A15);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0C01);
  spi.transaction(1,0,0,0,0,32,0,0);
  spi.set_mosi(1,0,16,0x0, 0x0F00);
  spi.transaction(1,0,0,0,0,32,0,0);

  local timeloop, innerhandler;
  local outidx = 1;

  function innerhandler(idx, dbytes, delay)
    node.task.post(function ()
      print(idx.." "..dbytes.." "..delay);
      spi.send(1, dbytes);
      print(node.heap());
      return timeloop(idx);
    end);
  end

  function timeloop (idx)
    idx = idx + 1;
    if datalist[outidx][idx] ~= nil then
      local dbytes, delay = unpack(datalist[outidx][idx]);
      if delay < 1 then
        return innerhandler(idx, dbytes, delay);
      else
        local _itmr = tmr.create();
        _itmr:alarm(delay, tmr.ALARM_SINGLE, function()
            _itmr=nil;
            return innerhandler(idx, dbytes, delay);
          end);
      end
    else
      outidx = outidx + 1;
      if datalist[outidx] ~= nil then
        if outerdelayms < 1 then
          return timeloop(0);
        else
          local _otmr = tmr.create();
          _otmr:alarm(outerdelayms, tmr.ALARM_SINGLE, function()
            _otmr=nil;
            return timeloop(0)
          end);
        end
      else
        if callback and type(callback) == "function" then
          return callback()
        end
        print("end of loop");
      end
    end
  end

  return timeloop(0);

end
