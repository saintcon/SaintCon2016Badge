
-- Call require('knightrider2')() from display.lua.
-- Event driven adapation of orignal: knightrider.lua

local moduleName = ...


local function start()

  local innerlist1 = {};
  innerlist1[1] = {0x0140,50};
  innerlist1[2] = {0x0240,1};
  innerlist1[3] = {0x0100,50};
  innerlist1[4] = {0x0340,1};
  innerlist1[5] = {0x0200,50};
  innerlist1[6] = {0x0440,1};
  innerlist1[7] = {0x0300,50};
  innerlist1[8] = {0x0540,1};
  innerlist1[9] = {0x0400,50};
  innerlist1[10] = {0x0640,1};
  innerlist1[11] = {0x0500,50};
  innerlist1[12] = {0x0740,1};
  innerlist1[13] = {0x0600,50};
  innerlist1[14] = {0x0840,1};
  innerlist1[15] = {0x0700,50};
  innerlist1[16] = {0x0740,1};
  innerlist1[17] = {0x0800,50};
  innerlist1[18] = {0x0640,1};
  innerlist1[19] = {0x0700,50};
  innerlist1[20] = {0x0540,1};
  innerlist1[21] = {0x0600,50};
  innerlist1[22] = {0x0440,1};
  innerlist1[23] = {0x0500,50};
  innerlist1[24] = {0x0340,1};
  innerlist1[25] = {0x0400,50};
  innerlist1[26] = {0x0240,1};
  innerlist1[27] = {0x0300,50};
  innerlist1[28] = {0x0140,1};
  innerlist1[29] = {0x0200,0};
  innerlist1[30] = {0x0100,0};

  local innerlist2 = {};
  innerlist2[1] = {0x0101,50};
  innerlist2[2] = {0x0201,1};
  innerlist2[3] = {0x0100,50};
  innerlist2[4] = {0x0301,1};
  innerlist2[5] = {0x0200,50};
  innerlist2[6] = {0x0401,1};
  innerlist2[7] = {0x0300,50};
  innerlist2[8] = {0x0501,1};
  innerlist2[9] = {0x0400,50};
  innerlist2[10] = {0x0601,1};
  innerlist2[11] = {0x0500,50};
  innerlist2[12] = {0x0701,1};
  innerlist2[13] = {0x0600,50};
  innerlist2[14] = {0x0801,1};
  innerlist2[15] = {0x0700,50};
  innerlist2[16] = {0x0701,1};
  innerlist2[17] = {0x0800,50};
  innerlist2[18] = {0x0601,1};
  innerlist2[19] = {0x0700,50};
  innerlist2[20] = {0x0501,1};
  innerlist2[21] = {0x0600,50};
  innerlist2[22] = {0x0401,1};
  innerlist2[23] = {0x0500,50};
  innerlist2[24] = {0x0301,1};
  innerlist2[25] = {0x0400,50};
  innerlist2[26] = {0x0201,1};
  innerlist2[27] = {0x0300,50};
  innerlist2[28] = {0x0101,1};
  innerlist2[29] = {0x0200,0};
  innerlist2[30] = {0x0100,0};

  local innerlist3 = {};
  innerlist3[1] = {0x0108,50};
  innerlist3[2] = {0x0208,1};
  innerlist3[3] = {0x0100,50};
  innerlist3[4] = {0x0308,1};
  innerlist3[5] = {0x0200,50};
  innerlist3[6] = {0x0408,1};
  innerlist3[7] = {0x0300,50};
  innerlist3[8] = {0x0508,1};
  innerlist3[9] = {0x0400,50};
  innerlist3[10] = {0x0608,1};
  innerlist3[11] = {0x0500,50};
  innerlist3[12] = {0x0708,1};
  innerlist3[13] = {0x0600,50};
  innerlist3[14] = {0x0808,1};
  innerlist3[15] = {0x0700,50};
  innerlist3[16] = {0x0708,1};
  innerlist3[17] = {0x0800,50};
  innerlist3[18] = {0x0608,1};
  innerlist3[19] = {0x0700,50};
  innerlist3[20] = {0x0508,1};
  innerlist3[21] = {0x0600,50};
  innerlist3[22] = {0x0408,1};
  innerlist3[23] = {0x0500,50};
  innerlist3[24] = {0x0308,1};
  innerlist3[25] = {0x0400,50};
  innerlist3[26] = {0x0208,1};
  innerlist3[27] = {0x0300,50};
  innerlist3[28] = {0x0108,1};
  innerlist3[29] = {0x0200,0};
  innerlist3[30] = {0x0100,0};

  local knightriderlst = {};
  knightriderlst[1] = innerlist1;
  knightriderlst[2] = innerlist1;
  knightriderlst[3] = innerlist1;
  knightriderlst[4] = innerlist2;
  knightriderlst[5] = innerlist2;
  knightriderlst[6] = innerlist2;
  knightriderlst[7] = innerlist3;
  knightriderlst[8] = innerlist3;
  knightriderlst[9] = innerlist3;
  -- Call with `start` call back if looping is desired.
  -- Remove callback if no looping is desired.
  require('displaylist')(knightriderlst, 0, start);
end

return function ()
  package.loaded[moduleName]=nil
  moduleName = nil
  start()
end
