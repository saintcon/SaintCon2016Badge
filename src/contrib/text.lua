-- Function to add scrolling text by magnusviri
-- Original Source https://www.reddit.com/r/SaintCon/comments/56xym5/saintcon_badge_discussion_2016/d8s5iz2
-- Modified by jkarras
-- * Use dynamic timers instead of timer 0. Static timers are depricated
-- * Added code to make standalone module.
-- * Removed the two set_mosi commands as they were no-ops in this implementation.
-- * Removed segment G from digit 7.

-- Usage:
-- mytext = require('text');
-- mytext('My text to scroll', 1000);
-- When your done with the object besure to set it to `nil` to reclaim RAM if needed.
-- mytext = nil;
--
-- Alternative Usage:
-- require('text')('My text to scroll', 1000);

local moduleName = ...

local char_matrix = {};
char_matrix[" "] = 0;
char_matrix["-"] = 1;
char_matrix["a"] = 1 + 2 + 4 + 0 + 16 + 32 + 64 + 0;
char_matrix["b"] = 1 + 2 + 4 + 8 + 16 + 0  + 0  + 0;
char_matrix["c"] = 0 + 2 + 4 + 8 + 0  + 0  + 64 + 0;
char_matrix["d"] = 1 + 0 + 4 + 8 + 16 + 32 + 0  + 0;
char_matrix["e"] = 1 + 2 + 4 + 8 + 0  + 0  + 64 + 0;
char_matrix["f"] = 1 + 2 + 4 + 0 + 0  + 0  + 64 + 0;
char_matrix["g"] = 1 + 2 + 0 + 8 + 16 + 32 + 64 + 0;
char_matrix["h"] = 1 + 2 + 4 + 0 + 16 + 32 + 0  + 0;
char_matrix["i"] = 0 + 2 + 4 + 0 + 0  + 0  + 0  + 0;
char_matrix["j"] = 0 + 0 + 4 + 8 + 16 + 32 + 0  + 0;
char_matrix["k"] = 1 + 2 + 4 + 0 + 16 + 32 + 0  + 0;
char_matrix["l"] = 0 + 2 + 4 + 8 + 0  + 0  + 0  + 0;
char_matrix["m"] = 0 + 0 + 4 + 0 + 16 + 0  + 64 + 0;
char_matrix["n"] = 1 + 0 + 4 + 0 + 16 + 0  + 0  + 0;
char_matrix["o"] = 0 + 2 + 4 + 8 + 16 + 32 + 64 + 0;
char_matrix["p"] = 1 + 2 + 4 + 0 + 0  + 32 + 64 + 0;
char_matrix["q"] = 1 + 2 + 0 + 0 + 16 + 32 + 64 + 0;
char_matrix["r"] = 1 + 0 + 4 + 0 + 0  + 0  + 0  + 0;
char_matrix["s"] = 1 + 2 + 0 + 8 + 16 + 0  + 64 + 0;
char_matrix["t"] = 1 + 2 + 4 + 8 + 0  + 0  + 0  + 0;
char_matrix["u"] = 0 + 2 + 4 + 8 + 16 + 32 + 0  + 0;
char_matrix["v"] = 0 + 0 + 4 + 8 + 16 + 0  + 0  + 0;
char_matrix["w"] = 0 + 2 + 0 + 8 + 0  + 32 + 0  + 0;
char_matrix["x"] = 1 + 2 + 4 + 0 + 16 + 32 + 0  + 0;
char_matrix["y"] = 1 + 2 + 0 + 8 + 16 + 32 + 0  + 0;
char_matrix["z"] = 1 + 0 + 4 + 8 + 16 + 32 + 64 + 0;
char_matrix["0"] = 0 + 2 + 4 + 8 + 16 + 32 + 64 + 0;
char_matrix["1"] = 0 + 0 + 0 + 0 + 16 + 32 + 0  + 0;
char_matrix["2"] = 1 + 0 + 4 + 8 + 0  + 32 + 64 + 0;
char_matrix["3"] = 1 + 0 + 0 + 8 + 16 + 32 + 64 + 0;
char_matrix["4"] = 1 + 2 + 0 + 0 + 16 + 32 + 0  + 0;
char_matrix["5"] = 1 + 2 + 0 + 8 + 16 + 0  + 64 + 0;
char_matrix["6"] = 1 + 2 + 4 + 8 + 16 + 0  + 64 + 0;
char_matrix["7"] = 0 + 0 + 0 + 0 + 16 + 32 + 64 + 0;
char_matrix["8"] = 1 + 2 + 4 + 8 + 16 + 32 + 64 + 0;
char_matrix["9"] = 1 + 2 + 0 + 8 + 16 + 32 + 64 + 0;
char_matrix["!"] = 0x00A0;
char_matrix["."] = 0x0080;


return function (str,speed)
  package.loaded[moduleName]=nil
  moduleName = nil
  spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 32, 8);
  local location = 0;
  local scrolltimer = tmr.create()
  scrolltimer:alarm(speed,tmr.ALARM_AUTO, function()
      spi.set_mosi(1,0,16, 0x0, 0x0900);
      spi.transaction(1,0,0,0,0,32,0,0);
      for i = 1, 8 do
        boo = ( i + location - 1 ) % string.len(str) + 1;
        letter = string.sub(str, boo, boo);
        print ( tostring(boo) .. letter );
        spi.send(1, {i * 256 + char_matrix[letter]});
      end
      location = ( location - 1 ) % string.len(str) + 2;
    end)
end
