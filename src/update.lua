--TODO look at using built in HTTP client https://nodemcu.readthedocs.org/en/dev/en/modules/http/

local moduleName = ...
local M = {}
local dlcount = 0
local ppcount = 0

local baseURL = 'http://badger.saintcon.org/master/lua/'
local updatefile = "update.json"
local updatejson = {}
local filelist = {}
local lastversion = {}


function M.dlfile(filename, callback)
  local mergedURL = baseURL..filename
  print(mergedURL)
  node.task.post(node.task.MEDIUM_PRIORITY, function()
    http.get(mergedURL, "Accept: */*\r\n", function(code, data)
      if (code == 200 ) then
        file.open(filename, "w+")
        file.write(data)
        file.flush()
        file.close()
        if callback and type(callback) == "function" then
          callback(filename)
        end
      else
        print('http error '..code)
        print(data)
      end
    end)
  end)
end

function M.updatecb(d)
    code,reason = node.bootreason()
    file.open(updatefile, "r")
    updatejson = cjson.decode(file.read())
    file.close()
    print("SC+BOOT="..reason)
    --val = rtcmem.read32(0)
    if updatejson.version ~= lastversion.version then
      --rtcmem.write32(0, 0)
      filelist = updatejson.filelist
      filename, cmd = next(filelist)
      print(filename,cmd)
      M.dlfile(filename, M.nextfile)
    else
      node.task.post(node.task.MEDIUM_PRIORITY, function ()
	--rtcmem.write32(0, val+1)
        print("No updates needed.")
        wifi.sta.eventMonStart()

        if scmqtt == nil then
            scmqtt = require('scmqtt')
        end
        scmqtt:init()

        package.loaded[moduleName]=nil
        moduleName = nil
        M = nil
      end)
    end
end

function M.nextfile (filename)
    filename, cmd = next(filelist, filename)
    if filename ~= nil then
      M.dlfile(filename, M.nextfile)
    else
      node.task.post(node.task.MEDIUM_PRIORITY, function ()
        print("download done")
        print("SC+UPGRADERESTART")
        node.restart()
      end)
    end
end

local function initdownload()
    file.remove('compilecheck')
    M.dlfile(updatefile, M.updatecb)
end

function M.run(connectwifi)
    print("SC+UPGRADEINIT")
    --Disconnect MQTT processing and destroy modules.
    if scmqtt ~= nil then
        scmqtt.unload()
        scmqtt = nil
    end

    if sc_wifievents ~= nil then
        wifi.sta.eventMonStop()
    end

    lastversion = require("fileutil").readcjson(updatefile)
    if lastversion == nil then
      lastversion = {}
      lastversion.version = 0
    end

    --TODO verify wifi connection. If non-existant make connection.
    if connectwifi == 1 then
        wifi.sta.eventMonStop()
        wifi.sta.eventMonReg(wifi.STA_GOTIP, initdownload)
        net.dns.setdnsserver("8.8.8.8",0)
        net.dns.setdnsserver("8.8.4.4",1)
        wifi.setmode(wifi.STATION)
        wifi.setphymode(wifi.PHYMODE_N)
        wifi.sta.eventMonStart()
        local wificreds = require("fileutil").readcjson("wificreds.json")
        if wificreds == nil then
            wificreds = {}
            wificreds.ssid = "SAINTCON"
            wificreds.password = "541n7c0n"
        end
        wifi.sta.config(wificreds.ssid,wificreds.password,1)
        initdownload()
    else
        initdownload()
    end

end

return M
