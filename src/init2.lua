MAX7219_REG_NOOP        = 0x00;
MAX7219_REG_DIGIT0      = 0x01;
MAX7219_REG_DIGIT1      = 0x02;
MAX7219_REG_DIGIT2      = 0x03;
MAX7219_REG_DIGIT3      = 0x04;
MAX7219_REG_DIGIT4      = 0x05;
MAX7219_REG_DIGIT5      = 0x06;
MAX7219_REG_DIGIT6      = 0x07;
MAX7219_REG_DIGIT7      = 0x08;
MAX7219_REG_DECODEMODE  = 0x09;
MAX7219_REG_INTENSITY   = 0x0A;
MAX7219_REG_SCANLIMIT   = 0x0B;
MAX7219_REG_SHUTDOWN    = 0x0C;
MAX7219_REG_DISPLAYTEST = 0x0F;


function sendByte (reg, data)
  spi.send(1,reg*256+data)
  tmr.delay(50)
end

function setup ()
  spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 16, 8);
  chipinit = {0x0C00,0x0B07,0x0F00,0x0A15,0x0C01,0x0000,0x0000}
  spi.send(1,chipinit);
  sendchipbyte(1,0x09FF);
  sendchipbyte(2,0x0900);

  tmr.stop(0);
end

function displayTest ()
  sendchipbyte (0,0x0F01);
  tmr.alarm(0, 400, tmr.ALARM_SINGLE, function ()
      sendchipbyte (0,0x0F00);
  end);
end

function displayClear ()
 for i=1,8 do
     sendByte(i,1);
 end
end

function returnOp(reg, data)
    return reg*256+data
end

--- Send byte to specific chip
--
-- @param chip which chip to send data.
-- @param data a number representing command+data.
function sendchipbyte(chip, data)
    if chip == 0 then
        -- set same data for both chips
        spi.set_mosi(1,0,16,data,data);
    elseif chip == 1 then
        -- set no-op (second chip) then data (first chip)
        spi.set_mosi(1,0,16,0x0000,data);
    elseif chip == 2 then
        -- set data (2nd chip) then no-op (first chip)
        spi.set_mosi(1,0,16,data,0x0000);
    else
        -- invalid choice send no-op. Should just do nothing here.
        spi.set_mosi(1,0,16,0x0000,0x0000);
    end
    spi.transaction(1,0,0,0,0,32,0,0);
end

setup();
displayTest();
displayClear();

--spi.set_mosi(1,0,16,MAX7219_REG_DIGIT0*256+0,MAX7219_REG_DIGIT1*256+1,MAX7219_REG_DIGIT2*256+2);
--spi.transaction(1,0,0,0,0,30,30,0);

sendchipbyte(1,returnOp(MAX7219_REG_DIGIT0,1));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT1,2));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT2,3));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT3,4));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT4,5));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT5,6));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT6,7));
sendchipbyte(1,returnOp(MAX7219_REG_DIGIT7,8));

sendchipbyte(2,returnOp(MAX7219_REG_DIGIT0,1));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT1,3));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT2,7));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT3,0x0F));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT4,0x1F));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT5,0x3F));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT6,0x7F));
sendchipbyte(2,returnOp(MAX7219_REG_DIGIT7,0xff));
