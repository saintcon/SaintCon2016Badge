#!/bin/bash -u

#Script forked from https://gist.github.com/kireevco/dfe1e80fb509d57b0f49

#TODO Make script build 'dev' and 'master' branches for upload.

######
PROJECT_NAME='saintcon_nodemcu_spiffs'
BUILD_DATE=$(TZ='America/Denver' date +%Y%m%d)
FWVERSION='BadgeBuildConf'
ESPINITDATA='esp_iot_sdk_v1.5.4.1_patch_20160704.zip'
ESPINITDATAADDR='0x3fc000'

x00000='0x00000.bin'
x10000='0x10000.bin'

BUILD_DIR="${CI_PROJECT_DIR}/builddir"
ARTIFACT_DIR="${CI_PROJECT_DIR}/build_artifacts"
NODEMCU_DIR="${BUILD_DIR}/nodemcu-firmware"
OUTPUT_FILE_NAME="${PROJECT_NAME}_${CI_BUILD_REF_NAME}-${CI_BUILD_REF}.bin"
"${GITLAB_CI:=false}"
######

################################################
# Building FW
prepare_build_environment () {
    mkdir "${BUILD_DIR}" -p || exit 1
    mkdir "${ARTIFACT_DIR}/bin" -p || exit 1
    mkdir "${ARTIFACT_DIR}/images" -p || exit 1
    cd "${BUILD_DIR}" || exit 1
    #Clone latest NodeMCU with SaintCon modifications.
    if [ ! -d "$NODEMCU_DIR/.git/" ]; then
        #Cache miss
        git clone --depth=1 --branch="${FWVERSION}" -- https://github.com/karrots/nodemcu-firmware.git "${NODEMCU_DIR}" || exit 1
    else
        #Cache hit
        cd "${NODEMCU_DIR}" || exit 1
        git fetch origin
        git reset --hard ${FWVERSION} || exit 1
        #Prevent the CI cache from spoiling new images.
        rm -rf "${NODEMCU_DIR:?}"/local/fs/*
        rm -rf "${NODEMCU_DIR:?}"/bin/*
        make spiffs-image-remove
    fi

    NODEMCU_BUILD="${NODEMCU_DIR}/$(git log --pretty=format:'%h' -n 1 .).cache"

    if [ ! -e "${NODEMCU_BUILD}" ]; then
        #Cache miss NodeMCU code changed.
        cd "${NODEMCU_DIR}" || exit 1
        touch "$(git log --pretty=format:'%h' -n 1 .)".cache
        make clean
    fi

    if [ ! -e "${NODEMCU_DIR}/esp-open-sdk" ]; then
        cd "${NODEMCU_DIR}" || exit 1
        #cache miss extract esp-open-sdk.tar.gz
        tar -zxvf tools/esp-open-sdk.tar.gz || exit 1
    fi

    if [ ! -e "${ARTIFACT_DIR}/images/nodemcu-firmware-image.bin" ]; then
        #cache miss extract esp_init_data_default.bin
        make sdk_patched || exit 1
        unzip -o "${NODEMCU_DIR}/cache/${ESPINITDATA}" esp_init_data_default.bin -d "${ARTIFACT_DIR}/images/" || exit 1
    fi

    if [ ! -e "${ARTIFACT_DIR}/images/luac.cross" ]; then
        #cache miss build luac.cross
        lua tools/cross-lua.lua || exit 1
        cp "${NODEMCU_DIR}/luac.cross" "${ARTIFACT_DIR}/bin/" || exit 1
    fi
}

build_nodemcu_image () {

    cp "${CI_PROJECT_DIR}"/src/* "${NODEMCU_DIR}/local/fs/"

    echo -n "http://fishtank.karras.co/${CI_BUILD_REF_NAME}/lua/" > "${NODEMCU_DIR}/local/fs/buildref.cfg"

    #Remove _score.dsp_ from the image build location.
    rm "${NODEMCU_DIR}/local/fs/score.dsp"

    cd "${NODEMCU_DIR}" || exit 1
    export PATH="${PATH}:${NODEMCU_DIR}/esp-open-sdk/sdk:${NODEMCU_DIR}/esp-open-sdk/xtensa-lx106-elf/bin"

    #Cache hit main image already built.
    if [ ! -e "${NODEMCU_DIR}/bin/${x00000}" ] || [ ! -e "${NODEMCU_DIR}/bin/${x10000}" ]; then
        make EXTRA_CCFLAGS="-DLUA_NUMBER_INTEGRAL -DBUILD_DATE='\"'${BUILD_DATE}'\"'" all || exit 1
    else
        make spiffs-image
    fi

    if [ ! -e "${ARTIFACT_DIR}/images/nodemcu-firmware-image.bin" ]; then
        #Create bare NodeMCU image without spiffs or flash init data.
        cd "${NODEMCU_DIR}/bin" || exit 1
        echo "Joining two firmware bin files"
        srec_cat  -output "${ARTIFACT_DIR}/images/nodemcu-firmware-image.bin" -binary "${x00000}" -binary -fill 0xff 0x00000 0x10000 "${x10000}" -binary -offset 0x10000 > srec_cat.log || exit 1
    fi

    #Copy SPIFFS image to build artifacts for later merging.
    cp "${NODEMCU_DIR}"/bin/0x*-*mb.bin "${ARTIFACT_DIR}/images/"
}

test_lua () {
    "${ARTIFACT_DIR}"/bin/luac.cross -p "${CI_PROJECT_DIR}"/src/*.lua || exit 1
}

merge_spiffs () {
    cd "${ARTIFACT_DIR}/images/" || exit 1

    main_firmware_offset=0x$(echo "obase=16; $(($(stat --format="%s" "${ARTIFACT_DIR}/images/nodemcu-firmware-image.bin")))" | bc)
    spiffaddr_re='^./(0x.*)-'
    if [[ $(ls ./*-32mb.bin) =~ ${spiffaddr_re} ]] ; then
        spiff_start_offset=${BASH_REMATCH[1]}
    fi
    echo "Main firmware offset: ${main_firmware_offset}"
    echo "Spiff start offset: ${spiff_start_offset}"
    echo "esp_init_data_default.bin: ${ESPINITDATAADDR}"
    #-redundant-bytes=ignore

    echo "Building final bin file"
    srec_cat -output "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" -binary \
      '(' "${ARTIFACT_DIR}/images/nodemcu-firmware-image.bin" -binary \
      "${ARTIFACT_DIR}/images/${spiff_start_offset}-32mb.bin" -binary -offset "${spiff_start_offset}" \
      "${ARTIFACT_DIR}/images/esp_init_data_default.bin" -binary -offset "${ESPINITDATAADDR}" ')' \
      -fill 0xff 0x00000 -maximum-address "${ARTIFACT_DIR}/images/esp_init_data_default.bin" -binary  >> srec_cat.log || exit 1

}

deploy () {
    #Copy firmware out of build dir for final storage.
#    cp "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" "${CI_PROJECT_DIR}/public/${CI_BUILD_REF_NAME}/latest-spiffs.bin" || exit 1
#    cp "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" "${CI_PROJECT_DIR}/public/${CI_BUILD_REF_NAME}/${OUTPUT_FILE_NAME}" || exit 1
#    scp -P 2122 "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" deploy@badger.saintcon.org:"/var/www/${CI_BUILD_REF_NAME}/images/latest-spiffs.bin" || exit 1
#    scp -P 2122 "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" deploy@badger.saintcon.org:"/var/www/${CI_BUILD_REF_NAME}/images/${OUTPUT_FILE_NAME}" || exit 1
    cp "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" "${ARTIFACT_DIR}/images/latest-spiffs.bin"
    s3cmd put --rr -P "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" "s3://saintcon2016badge.karras.co/${CI_BUILD_REF_NAME}/images/"
    s3cmd put --rr -P "${ARTIFACT_DIR}/images/${OUTPUT_FILE_NAME}" "s3://saintcon2016badge.karras.co/${CI_BUILD_REF_NAME}/images/latest-spiffs.bin"
}

clean () {
    if [[ "$GITLAB_CI" = "true" ]] ; then
        echo "done"
    else
        echo "Cleaning build files. 15 second delay... "
        sleep 15

        echo "Cleaning up build directory"
        rm -rf "${BUILD_DIR:?}"
    fi
    exit
}

while [ "$1" != "" ]; do
    case $1 in
        --prepare|prepare)
            echo "IMAGE: Prepare build environment..."
            prepare_build_environment
            exit
            ;;
        --build|build)
            echo "IMAGE: Build base image..."
            build_nodemcu_image
            merge_spiffs
            clean
            ;;
        --test|test)
            echo "IMAGE: Test lua syntax..."
            test_lua
            clean
            ;;
        --deploy|deploy|review)
            echo "IMAGE: Deploy image+spiffs.."
            deploy
            clean
            ;;
        *)
            echo "IMAGE: No stage specified."
            exit
    esac
done

exit $?
