#!/bin/bash -x

build_update_json () {
    cd "${CI_PROJECT_DIR:=.}/src/" || exit 1

    #Create JSON update list.
    echo -n '{ "version": "'${CI_BUILD_REF}'",' > update.json
    echo -n ' "filelist": {' >> update.json
    for f in *.json; do
        echo "Processing $f ..."
        echo -n '"'$(basename $f)'":"'download'"' >> update.json
    done
    for f in *.lua; do
        echo "Processing $f ..."
        echo -n '"'$(basename $f)'":"'download'"' >> update.json
    done
    echo -n '} }' >> update.json


    cp update.json update2.json
    sed 's/""/","/g' update2.json > update.json

    rm update2.json
}

deploy_lua_source ()
{
    #Deploy Lua and JSON update list.
    s3cmd put --rr -P --recursive ./* "s3://saintcon2016badge.karras.co/${CI_BUILD_REF_NAME}/lua/"
    #scp -P 2122 -r ./* deploy@badger.saintcon.org:"/var/www/${CI_BUILD_REF_NAME}/lua"
}

while [ "$1" != "" ]; do
    case $1 in
        --prepare|prepare)
            echo "LUA: No prepare operations."
            exit
            ;;
        --test|test)
            echo "LUA: No test operations."
            exit
            ;;
        --build|build)
            echo "LUA: Build update.json"
            build_update_json
            exit
            ;;
        --deploy|deploy|review)
            echo "LUA: Deploy update.json + lua files."
            build_update_json
            deploy_lua_source
            exit
            ;;
        *)
            echo "LUA: No stage specified."
            exit
    esac
done

exit $?
