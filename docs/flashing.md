There are multiple tools available for flashing the initial image to the D1 Mini. Find the section below which best fits your needs.

[TOC]

#Flash the badge

##Visit flash station in HHV

Located in various places in HHV you'll find Raspberry Pi's setup to flash the latest SaintCon image. Disconnect the D1 mini from the badge before flashing. See instructions at flash station.

##Use your own computer
1. Disconnect D1 Mini from badge.
1. Install drivers for the CH340g chip.
    1. [MacOS](https://github.com/adrianmihalko/ch340g-ch34g-ch34x-mac-os-x-driver)
    1. [Windows](https://www.wemos.cc/downloads)
    1. Linux - Just works
1. Install the flashing tool you'll be using.
    1. [esptool.py](https://github.com/themadinventor/esptool) - (Mac, Win, Linux).
    1. [Official Expressif Download Tool](https://espressif.com/sites/default/files/tools/flash_download_tools_v3.4.2_win.zip) - Not recommended.
1. Download the latest badge firmware [link goes here](http://saintcon2016badge.karras.co/master/images/latest-spiffs.bin)
1. Plug D1 mini into computer via USB port and follow instructions specific to your flashing tool.

    `esptool.py --port <serial-port-of-ESP8266> --baud 230400 write_flash -fm dio -fs 32m 0x00000 <badge-firmware>.bin`

    Lower baud rate to 115200 if you run into flashing issues.

Check the [esptool flash modes documentation](https://github.com/themadinventor/esptool#flash-modes) for details and other options.

#Upload custom Lua code

Edit the Lua code in your favorite text editor. Upload to the NodeMCU board with a tool below. The tools below can also be used to retrieve the LUA code from the badge.

* [nodemcu-uploader.py](https://github.com/kmpm/nodemcu-uploader) - Python based upload tool. CLI oriented easy to use.
* [ESPlorer](https://github.com/4refr0nt/ESPlorer) - A simple Russian made IDE for uploading and modifying code on the ESP chip.

More general NodeMCU coding information can be found here:
[http://nodemcu-firmware.readthedocs.io/en/latest/en/upload/](http://nodemcu-firmware.readthedocs.io/en/latest/en/upload/).

#Community Extras

Below are resources provided by the saintcon community.

* [Binary to Hex converter](hexconverter.xlsx) - Easily convert binary to HEX to program badge segments. - Jesse Howerton
