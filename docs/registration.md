# Badge Hackers Challenge Registration

While registering for Hackers Challenge you'll be prompted to enter your badge ID. After registering your badge will show your current game score from the last 30 seconds. The badge ID can be obtained from the two places listed below:

1. Flash station output after flashing. **link_code/UUID:XXXXXXXXXXXX**

1. Bootup message on the D1 mini. Plug USB into serial. **SC+UUID=XXXXXXXXXXXX**
```
NodeMCU 1.5.4.1 build 20161005 powered by Lua 5.1.4 on SDK 1.5.4.1(39cb9a32)
Waiting 5 sec....
> SC+UUID=XXXXXXXXXXXX
SC+MAC=5c:cf:7f:BB:YY:XX
SC+FLASHID=1458488
Registering events:
STA_GOTIP
STA_IDLE
STA_CONNECTING
STA_WRONG_PASSWORD
STA_NO_AP_FOUND
STA_CONNECT_FAIL
Starting wifi event monitoring
```
