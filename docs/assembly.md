# Badge Assembly

Look the board over and locate the listed part numbers. Note any items that have a specific orientation or polarization.

If you have never soldered before review the guide below for instructions.

* [How To Solder](https://learn.sparkfun.com/tutorials/how-to-solder---through-hole-soldering)


## Where do I start

The following items are in your conference bag:

1. Badge PCB.
1. Bag of resistors, capacitors, fuses, headers,
1. D1 Mini ESP8266 development board.
1. Bag containing 2x male headers, 2x female headers, and 2x stacking headers.
1. USB battery
1. Screw or washer to exchange for the following at HHV:
    1. MAX7219 LED driver IC.
    1. 2x 4 digit LEDs (yellow, green, red, white, and eye searing blue).
1. See photos [below](#finished-examples)

## Assembly gotchas

* Don't directly solder the D1 Mini or the MAX 7219 chip to the board. Solder the included headers and IC socket in place first.
* Solder small items like diodes and resistors first.
* Solder one pin on the IC socket or header, then heat that pin up while using a free hand to straighten the part. **Don't touch any hot parts!!**
* Place female pins on the board and male or stacking headers on the D1 mini. Check orientation first!
* **When attaching the D1 mini to the board the FCC logo should be face up.**  See [side view](/img/badge_side_view.jpeg) and [pins/socket](/img/badge_wemo_socket.jpeg).
* Which side of the diode is positive? [https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity](https://learn.sparkfun.com/tutorials/polarity/diode-and-led-polarity)
* Solder LEDs directly to the badge. Use of headers will lead to loose connections.

## Bill of Materials
| Qty | Board Label | Description |
| --- | ----------- | ----------- |
| 1   | C1          | [0.1uf Ceramic Capacitor](http://www.taydaelectronics.com/10-x-0-1uf-50v-ceramic-disc-capacitor-pkg-of-10.html) |
| 1   | C3          | [10uf Electrolytic Capacitor](http://www.taydaelectronics.com/10uf-25v-105c-radial-electrolytic-capacitor-5x11mm.html) ***polarized*** |
| 1   | D1          | [1N4148 Signal Diode](http://www.taydaelectronics.com/1n4148-switching-signal-diode.html) ***polarized*** |
| 1   | iset        | Resistor [which resistor?](#resistor-selection) |
| 2   | F5 F6       | [500ma PolySwitch PTC Fuse](http://www.taydaelectronics.com/poly-switch-resettable-rxef-series-30v-0-50a.html) |
| 1   | U1          | [24 pin IC Socket](http://www.taydaelectronics.com/24-pin-dip-ic-sockets-adaptor-solder-type.html) ***polarized*** |
| 1   | U1          | [MAX 7219 LED Driver](http://www.taydaelectronics.com/max7219-7-segment-dot-point-common-cathode-serially-interfaced-display-driver-pdip-24.html) ***polarized*** |
| 2   | U16 U17     | [4 digit 7-segment LED display](https://www.sparkfun.com/products/11405) ***polarized*** |
| 1   | U58         | [Wemos D1 Mini](https://www.wemos.cc/product/d1-mini.html) + included headers ***polarized***|
| 5   | various     | [8x1 Female headers](http://www.taydaelectronics.com/8-pin-2-54-mm-single-row-female-pin-header.html) |

## Resistor selection

The iset resistor is used to limit the current draw of the LEDs. The resistor must be chosen with the LED forward voltage and forward current in mind. Use the table below to select which of the two resistors you need to use on your badge. **There are limited quantities of the various colors. Don't solder the resistor until you have your LEDs in hand!**

| LED Color | iset resistor value |
| :-------: | :-----------------: |
| Green     | 18k ohm             |
| White     | 30k ohm             |
| Blue      | 30k ohm             |
| Yellow    | 30k ohm             |
| Red       | 30k ohm             |

[Resistor Color Code Calculator](http://www.digikey.com/en/resources/conversion-calculators/conversion-calculator-resistor-color-code-4-band)

## Schematic

[![Schematic](/img/badge_schematic.svg)](/img/badge_schematic.svg)

## Finished examples

[![MAX Orientation](/img/IMG_20160929_204111.jpg)](/img/IMG_20160929_204111.jpg)
[![Finished Badge](/img/IMG_20160929_213212.jpg)](/img/IMG_20160929_213212.jpg)
[![Badge side view](/img/badge_side_view.jpeg)](/img/badge_side_view.jpeg)
[![Badge wemo socket](/img/badge_wemo_socket.jpeg)](/img/badge_wemo_socket.jpeg)
[![Badge PCB Drawing](/img/badge-pcb.PNG)](/img/badge-pcb.PNG)
