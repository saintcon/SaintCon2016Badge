## Test MQTT Push

### Use Screen to Find UUID on Badge Boot
```
# screen /dev/ttyUSB0  115200
Wating 5 Sec ...
SC+UUID=<uuid>
```

### Add UUID to `sample.json`
```
[{
    "UUID": "<uuid>"
}]
```

### Run push.py to push update to badge
```
$ python push.py --score 100 --brightness med
```