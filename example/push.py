#!/usr/bin/python
# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
import json
import argparse
import os
import sys
from pprint import pprint
from time import sleep


def on_connect(client, userdata, flags, rc):
    print("Connected..." + str(rc) + "\n")


def on_disconnect(client, userdata, rc):
    print("Disconnected...\n")


def on_log(client, userdata, level, buf):
    print(buf)


def getScriptPath():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='Testing application for MQTT and the badge')

    parser.add_argument(
        '--infile', '-i',
        help='Filename data is read from to send to badge.',
        default=os.path.join(getScriptPath(), 'sample.json')
    )
    
    parser.add_argument(
        '--score', '-s',
        help='Current Score.',
        default=0
    )
    parser.add_argument(
        '--brightness', '-b',
        help='Screen Brightness.',
        default=0
    )

    args = parser.parse_args()

    with open(args.infile) as data_file:
        jsondata = json.load(data_file)

    mqttc = mqtt.Client("python_pub", False)
    mqttc.on_connect = on_connect
    mqttc.on_disconnect = on_disconnect
    mqttc.on_log = on_log
    mqttc.username_pw_set("null", "password")
    #mqttc.tls_insecure_set(True)
    #mqttc.tls_set("daddy.pem")
    mqttc.connect("badger.saintcon.org", 1883, 60)

    mqttc.loop_start()
    sleep(1)
    for badge in jsondata:
        mqttc.publish("to/badge/" + badge['UUID'] +
                      "/hackerscore", args.score, 1, False)
        mqttc.publish("to/badge/" + badge['UUID'] +
                      "/brightness", args.brightness, 1, False)

    mqttc.disconnect()
    mqttc.loop_stop()
